## Tags ##
tags = {
  Environment = "testing"
}

## Provider Variables ##
subscription_id = "b15e3114-37af-4463-8659-786dc5cc0bff"

## Resource Group Variables ##
resource_group_name = "net_v2_azure_resource_group"
location            = "west europe"

## Virtual Network Variables ##
vnet_name    = "azure_net"
network_cidr = "10.0.0.0/16"

## Public Subnet Variables ##
public_subnets = {
  public_subnet_1 = {
    name                        = "public_subnet_1"
    address_prefixes            = ["10.0.1.0/24"]
    network_security_group_name = "public_subnet_nsg_1"
    disable_network_policies = {
      for_endpoint = false
      for_services = false
    }
    public_subnet_delegations = {
      delegation_hosting = {
        name                   = "delegation_hosting"
        svc_delegation_name    = "Microsoft.Web/hostingEnvironments"
        svc_delegation_actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
      }
    }
    public_subnet_security_rules = {
      allow-http = {
        name                       = "allow-http"
        priority                   = 100
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      }
      allow-https = {
        name                       = "allow-https"
        priority                   = 101
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      }
    }
  }
}

## Private Subnet Variables ##
private_subnets = {
  private_subnet_compute = {
    name             = "private_subnet_compute"
    address_prefixes = ["10.0.2.0/24"]
    disable_network_policies = {
      for_endpoint = false
      for_services = false
    }
    private_subnet_delegations = {
      delegation_compute = {
        name                   = "delegation_compute"
        svc_delegation_name    = "Microsoft.BareMetal/AzureVMware"
        svc_delegation_actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
      }
    }
  }

  private_subnet_db = {
    name             = "private_subnet_db"
    address_prefixes = ["10.0.3.0/24"]
    disable_network_policies = {
      for_endpoint = true
      for_services = true
    }
    private_subnet_delegations = {
      delegation_db = {
        name                   = "delegation_db"
        svc_delegation_name    = "Microsoft.AzureCosmosDB/clusters"
        svc_delegation_actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
      }
    }
  }
}

## NAT Gateway ##
public_subnet_gateway_ip = {
  name          = "public_subnet_ip"
  allocation    = "Static"
  sku           = "Standard"
  prefix_name   = "public_subnet_ip_prefix"
  prefix_length = 30
}

public_subnet_gateway = {
  name    = "public_subnet_gateway"
  sku     = "Standard"
  timeout = 10
}

public_subnet_gateway_ip_prefix_enabled = false