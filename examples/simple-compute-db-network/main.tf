provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

module "azure-base-networks" {
  source = "../.."
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip                = var.public_subnet_gateway_ip
  public_subnet_gateway                   = var.public_subnet_gateway
  public_subnet_gateway_ip_prefix_enabled = var.public_subnet_gateway_ip_prefix_enabled
}