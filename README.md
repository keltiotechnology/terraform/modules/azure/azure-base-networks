<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Azure Virtual Network

Terraform module which create the following resources:

- a Virtual network that contains
  - user defined public subnets
  - user defined private subnets
  - user defined NAT gateway

## Module usage

```hcl
provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
}

module "azure-base-networks" {
  source = "../.."
  tags   = var.tags

  ## Resource Group ##
  resource_group_name = var.resource_group_name
  location            = var.location

  ## Virtual Network Variables ##
  vnet_name    = var.vnet_name
  network_cidr = var.network_cidr

  ## Public Subnet Variables ##
  public_subnets = var.public_subnets

  ## Private Subnet Variables ##
  private_subnets = var.private_subnets

  ## NAT Gateway Variables ##
  public_subnet_gateway_ip                = var.public_subnet_gateway_ip
  public_subnet_gateway                   = var.public_subnet_gateway
  public_subnet_gateway_ip_prefix_enabled = var.public_subnet_gateway_ip_prefix_enabled
}
```
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 2.82.0 |

## Resources

| Name | Type |
|------|------|
| [azurerm_nat_gateway.public_subnet_gateway](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/nat_gateway) | resource |
| [azurerm_nat_gateway_public_ip_association.public_nat_ip_assoc](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/nat_gateway_public_ip_association) | resource |
| [azurerm_network_security_group.nsg_public](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_security_group) | resource |
| [azurerm_public_ip.public_subnet_gateway_ip](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip) | resource |
| [azurerm_public_ip_prefix.public_subnet_ip_prefix](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip_prefix) | resource |
| [azurerm_resource_group.network_res_group](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |
| [azurerm_subnet.private_subnet](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) | resource |
| [azurerm_subnet.public_subnet](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) | resource |
| [azurerm_subnet_nat_gateway_association.public_subnet_gw_assoc](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet_nat_gateway_association) | resource |
| [azurerm_subnet_network_security_group_association.public_subnet_assoc](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet_network_security_group_association) | resource |
| [azurerm_virtual_network.azure_vnet](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network) | resource |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 2.82.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_location"></a> [location](#input\_location) | Azure region where the resource group will be created | `string` | `"france central"` | no |
| <a name="input_network_cidr"></a> [network\_cidr](#input\_network\_cidr) | CIDR Block for network | `string` | n/a | yes |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | Private Subnet informations map | `any` | n/a | yes |
| <a name="input_public_subnet_gateway"></a> [public\_subnet\_gateway](#input\_public\_subnet\_gateway) | Public subnet NAT gateway properties | `map(any)` | <pre>{<br>  "name": "public_subnet_gateway",<br>  "sku": "Standard",<br>  "timeout": 10<br>}</pre> | no |
| <a name="input_public_subnet_gateway_ip"></a> [public\_subnet\_gateway\_ip](#input\_public\_subnet\_gateway\_ip) | Public IP properties for the NAT gateway | `map(any)` | <pre>{<br>  "allocation": "Static",<br>  "name": "public_subnet_ip",<br>  "prefix_length": 30,<br>  "prefix_name": "public_subnet_ip_prefix",<br>  "sku": "Standard"<br>}</pre> | no |
| <a name="input_public_subnet_gateway_ip_prefix_enabled"></a> [public\_subnet\_gateway\_ip\_prefix\_enabled](#input\_public\_subnet\_gateway\_ip\_prefix\_enabled) | Set whether to use public IP prefix for subnet gateway | `bool` | `false` | no |
| <a name="input_public_subnets"></a> [public\_subnets](#input\_public\_subnets) | Public Subnet informations map | `any` | n/a | yes |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Name of the resource group | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Resources Tags | `map(any)` | n/a | yes |
| <a name="input_vnet_name"></a> [vnet\_name](#input\_vnet\_name) | Virtual Network Name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_gateway_public_ip"></a> [gateway\_public\_ip](#output\_gateway\_public\_ip) | Outputs NAT Gateway public IP |
| <a name="output_private_subnets"></a> [private\_subnets](#output\_private\_subnets) | Outputs each private subnet properties |
| <a name="output_public_subnets"></a> [public\_subnets](#output\_public\_subnets) | Outputs each public subnet properties |
| <a name="output_resource_group"></a> [resource\_group](#output\_resource\_group) | Outputs the resource group name |
| <a name="output_virtual_network"></a> [virtual\_network](#output\_virtual\_network) | Outputs the virtual network ID and the CIDR |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->