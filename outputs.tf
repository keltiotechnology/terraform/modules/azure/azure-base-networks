output "resource_group" {
  value = {
    name     = azurerm_resource_group.network_res_group.name
    location = azurerm_resource_group.network_res_group.location
  }
  description = "Outputs the resource group name"
}

output "virtual_network" {
  value = {
    id   = azurerm_virtual_network.azure_vnet.id
    cidr = azurerm_virtual_network.azure_vnet.address_space
  }
  description = "Outputs the virtual network ID and the CIDR"
}

output "public_subnets" {
  value       = { for k, v in azurerm_subnet.public_subnet : k => v }
  description = "Outputs each public subnet properties"
}

output "private_subnets" {
  value       = { for k, v in azurerm_subnet.private_subnet : k => v }
  description = "Outputs each private subnet properties"
}

output "gateway_public_ip" {
  value       = try(azurerm_public_ip.public_subnet_gateway_ip.0.ip_address, null)
  description = "Outputs NAT Gateway public IP"
}