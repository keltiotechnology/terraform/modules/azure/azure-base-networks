resource "azurerm_virtual_network" "azure_vnet" {
  name                = var.vnet_name
  resource_group_name = var.resource_group_name
  location            = var.location
  address_space       = [var.network_cidr]
  tags                = var.tags
  depends_on          = [azurerm_resource_group.network_res_group]
}

resource "azurerm_subnet" "public_subnet" {
  for_each = var.public_subnets

  name                 = each.value.name
  address_prefixes     = each.value.address_prefixes
  resource_group_name  = azurerm_virtual_network.azure_vnet.resource_group_name
  virtual_network_name = azurerm_virtual_network.azure_vnet.name

  private_endpoint_network_policies_enabled     = try(each.value.disable_network_policies["for_endpoint"], null)
  private_link_service_network_policies_enabled = try(each.value.disable_network_policies["for_services"], null)

  dynamic "delegation" {
    for_each = each.value.public_subnet_delegations

    iterator = deleg
    content {
      name = deleg.value.name
      service_delegation {
        name    = deleg.value.svc_delegation_name
        actions = deleg.value.svc_delegation_actions
      }
    }
  }
}

resource "azurerm_subnet" "private_subnet" {
  for_each = var.private_subnets

  name                 = each.value.name
  address_prefixes     = each.value.address_prefixes
  resource_group_name  = azurerm_virtual_network.azure_vnet.resource_group_name
  virtual_network_name = azurerm_virtual_network.azure_vnet.name

  private_endpoint_network_policies_enabled     = try(each.value.disable_network_policies["for_endpoint"], null)
  private_link_service_network_policies_enabled = try(each.value.disable_network_policies["for_services"], null)

  dynamic "delegation" {
    for_each = each.value.private_subnet_delegations

    iterator = deleg
    content {
      name = deleg.value.name
      service_delegation {
        name    = deleg.value.svc_delegation_name
        actions = deleg.value.svc_delegation_actions
      }
    }
  }
}

resource "azurerm_network_security_group" "nsg_public" {
  for_each = var.public_subnets

  name                = each.value.network_security_group_name
  location            = var.location
  resource_group_name = azurerm_virtual_network.azure_vnet.resource_group_name

  dynamic "security_rule" {
    for_each = each.value.public_subnet_security_rules

    iterator = rule
    content {
      name                       = rule.value.name
      priority                   = rule.value.priority
      direction                  = rule.value.direction
      access                     = rule.value.access
      protocol                   = rule.value.protocol
      source_port_range          = rule.value.source_port_range
      destination_port_range     = rule.value.destination_port_range
      source_address_prefix      = rule.value.source_address_prefix
      destination_address_prefix = rule.value.destination_address_prefix
    }
  }
  tags = var.tags
}

#Link public subnet with the previously created network security group association
resource "azurerm_subnet_network_security_group_association" "public_subnet_assoc" {
  for_each = var.public_subnets

  subnet_id                 = azurerm_subnet.public_subnet[each.key].id
  network_security_group_id = azurerm_network_security_group.nsg_public[each.key].id
}
