resource "azurerm_public_ip" "public_subnet_gateway_ip" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  name                = var.public_subnet_gateway_ip.name
  location            = azurerm_resource_group.network_res_group.location
  resource_group_name = azurerm_resource_group.network_res_group.name
  allocation_method   = var.public_subnet_gateway_ip.allocation
  sku                 = var.public_subnet_gateway_ip.sku
  tags                = var.tags
}

resource "azurerm_public_ip_prefix" "public_subnet_ip_prefix" {
  count = length(var.public_subnets) > 0 && var.public_subnet_gateway_ip_prefix_enabled ? 1 : 0

  name                = var.public_subnet_gateway_ip.prefix_name
  location            = azurerm_resource_group.network_res_group.location
  resource_group_name = azurerm_resource_group.network_res_group.name
  prefix_length       = var.public_subnet_gateway_ip.prefix_length
  tags                = var.tags
}

resource "azurerm_nat_gateway" "public_subnet_gateway" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  name                    = var.public_subnet_gateway.name
  location                = azurerm_resource_group.network_res_group.location
  resource_group_name     = azurerm_resource_group.network_res_group.name
  sku_name                = var.public_subnet_gateway.sku
  idle_timeout_in_minutes = var.public_subnet_gateway.timeout
  tags                    = var.tags
}

#Link NAT Gateway with the previously created public IP
resource "azurerm_nat_gateway_public_ip_association" "public_nat_ip_assoc" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  nat_gateway_id       = azurerm_nat_gateway.public_subnet_gateway.0.id
  public_ip_address_id = azurerm_public_ip.public_subnet_gateway_ip.0.id
}

resource "azurerm_nat_gateway_public_ip_prefix_association" "public_nat_ip_prefix_assoc" {
  count = length(var.public_subnets) > 0 && var.public_subnet_gateway_ip_prefix_enabled ? 1 : 0

  nat_gateway_id      = azurerm_nat_gateway.public_subnet_gateway.0.id
  public_ip_prefix_id = azurerm_public_ip_prefix.public_subnet_ip_prefix.0.id
}

#Link all of the public subnets with the previously created NAT Gateway
resource "azurerm_subnet_nat_gateway_association" "public_subnet_gw_assoc" {
  for_each = var.public_subnets

  subnet_id      = azurerm_subnet.public_subnet[each.key].id
  nat_gateway_id = azurerm_nat_gateway.public_subnet_gateway.0.id
}
